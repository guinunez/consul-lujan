set :deploy_to, deploysecret(:deploy_to)
set :server_name, deploysecret(:server_name)
set :db_server, deploysecret(:db_server)
set :branch, :master
set :ssh_options, port: 2287
set :stage, :production
set :rails_env, :production

#server '190.220.134.70', user: 'deploy', roles: %w{app db web}

server deploysecret(:server1), user: deploysecret(:user), roles: %w(web app db)
#server deploysecret(:server2), user: deploysecret(:user), roles: %w(web app db importer cron background)
#server deploysecret(:server3), user: deploysecret(:user), roles: %w(web app db importer)
#server deploysecret(:server4), user: deploysecret(:user), roles: %w(web app db importer)
