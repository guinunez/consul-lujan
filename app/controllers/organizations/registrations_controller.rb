class Organizations::RegistrationsController < Devise::RegistrationsController

  invisible_captcha only: [:create], honeypot: :address, scope: :user
  skip_before_action :authenticate_scope!, only: [:edit, :update]
  before_action :authenticate_user!, only: [:edit, :update]
  

  def new
    super do |user|
      user.build_organization
    end
  end

  def edit
    @organization = Organization.new
  end

  def update
    org = current_user.create_organization(update_params)
    org.verify
    redirect_to new_proposal_path
  end

  def success
  end

  def create
    build_resource(sign_up_params)
    if resource.valid?
      super do |user|
        # Removes unuseful "organization is invalid" error message
        user.errors.messages.delete(:organization)
      end
    else
      render :new
    end
  end

  protected

    def after_inactive_sign_up_path_for(resource)
      organizations_sign_up_success_path
    end

  private

    def sign_up_params
      params.require(:user).permit(:email, :password, :phone_number, :password_confirmation, :terms_of_service,
                                   organization_attributes: [:name, :responsible_name])
    end

    def update_params
      params.require(:organization).permit(:name, :responsible_name)
    end

end
