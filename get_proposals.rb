require 'csv'

start_date = Date.new(2018, 07, 12)
end_date = Date.today
is_approved = true

proposals = Proposal.includes([:author, :geozone]).where(
    "is_approved = :is_approved AND created_at >= :start_date AND proposals.created_at <= :end_date", 
    {is_approved: is_approved, start_date: start_date, end_date: end_date}
).includes('author', 'geozone')

CSV.open('proposals.csv', 'w') do |csv|
    csv << ['titulo','organizacion','estado','geozone','latitud','longitud']
    proposals.each do |p|
        csv << [
            p.title,
            p.author.name,
            p.is_approved ? 'aprobado' : 'rechazado',
            p.geozone ? p.geozone.name : '',
            p.map_location.latitude,
            p.map_location.longitude]
    end
end