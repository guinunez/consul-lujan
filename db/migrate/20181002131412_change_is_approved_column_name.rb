class ChangeIsApprovedColumnName < ActiveRecord::Migration
  def change
    rename_column :proposals, :isApproved, :is_approved
    rename_column :proposals, :isArchived, :is_archived
  end
end
