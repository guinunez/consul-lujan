class AddIsArchivedProposal < ActiveRecord::Migration
  def change
    add_column :proposals, :isArchived, :boolean, default: false
  end
end
