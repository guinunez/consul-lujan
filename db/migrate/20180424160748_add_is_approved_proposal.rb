class AddIsApprovedProposal < ActiveRecord::Migration
  def change
    add_column :proposals, :isApproved, :boolean, default: false
  end
end
