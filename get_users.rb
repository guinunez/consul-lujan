require 'csv'

users = User.all

CSV.open('users.csv', 'w') do |csv|
    csv << ['Email', 'Nombre']
    users.each do |user|
        email = user.email || user.oauth_email || user.unconfirmed_email
        name = user.username

        csv << [email.to_s, name.to_s] if !email.blank?
    end
end